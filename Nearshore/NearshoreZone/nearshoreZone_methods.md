__Creating Nearshore Region__
=========================
**Katie Gale, 20 Jan 2017, updated 17 March 2017**

There are probably extra steps in this protocol, but each step takes a long time and without some of the erase/dissolve steps ArcMap just hangs and crashes. 

Summary
----
* The coastline was buffered to 1 or 2 km, and all areas where the buffer touched itself to form a "donut hole" were enclosed (e.g., when an inlet mouth narrows, the wider inlet was completely contained).
* Areas with depths of 20 m or shallower were selected, buffered by 250 m to reduce harsh edges, and then appended to the coastal buffer.
* The final units are either "2 km buffer with 20 m depth with 250 m buffer" or "1 km buffer with 20 m depth with 250 m buffer" 
* There were two areas identified that were not closed in following the above steps, because the channel mouths were a bit more than 4 km wide. These areas were manually closed to capture the internal waters of the area.
 * This was done for two areas, Squally Channel (between Gil and Campania Islands) and Portland Inlet (between Truro and Wales Islands)  

# Technical Steps
## Making buffered coastline (i.e., delineating all area a certain distance from shore)
* This was done for 1km and 2km distances from shore. Anywhere it says 1km in these file names, could also be 2km

1) Start with Coastline (`DFO_BC_CHS_COASTLIN_AND_BC_BODY.shp`)

2) Make copy of `DFO_BC_CHS_COASTLIN_AND_BC_BODY.shp` to `Coastline_1kmBuffer.shp`

3) Start edit session, select all features of `Coastline_1kmBuffer.shp` and create buffer of 1000 m [or 2000 m].

4) Use Feature to Polygon (Data Management > Features) to fill in "donut holes" in the layer. Save as `Coastline_1kmBuffer_FTP.shp`.

5) Erase (Analysis Tools > Overlay) `Coastline_1kmBuffer_FTP.shp` with `DFO_BC_CHS_COASTLIN_AND_BC_BODY.shp` to get `Coastline_1kmBuffer_FTPErase.shp`.

6) Dissolve (Data Management > Generalization) `Coastline_1kmBuffer_FTPErase.shp` to get `Coastline_1kmBuffer_FTPEraseDissolve.shp` and save a copy called `Nearshore_1km.shp`


## Adding a depth contour to the buffered coastline (i.e., adding all areas 20 m deep) 
7) Take raster layer `bc_eez_100m.tif`

8) Display `bc_eez_100m.tif` as categories 0-20, 20.1-50, 50.1+

9) Use Reclass (Spatial Analyst > Reclass) to change `bc_eez_100m.tif` continuous raster to a categorical raster with the above categories (`bc_100m-recls.tiff`)

10) Use Raster to Polygon (Conversion > From Raster) to convert `bc_100m-recls.tiff` to `bc_100m-recls-20-50.shp`

11) Extract only the 20 m band (`bc_100m-recls_20m.shp`)

12) Buffer `bc_100m-recls_20m.shp` by 250 m to reduce harsh edges. Save as `bc_100m-recls_20m_250mbuffer.shp`.

13) Erase `bc_100m-recls_20m_250mbuffer.shp` with `DFO_BC_CHS_COASTLIN_AND_BC_BODY.shp` to get `Depth_20m250mbuffer.shp`

14) Use Selection > Select by Location to select the pieces of `Depth_20m250mbuffer.shp` that INTERSECT with `Nearshore_1km`, and extract those pieces to new layer (`Depth_20m250mbuffer_touching1kmbuffer.shp`)

15) Use Union (Analysis Tools > Overlay) to combine `Depth_20m250mbuffer_touching1kmbuffer.shp` and `Nearshore_1km` with option DO NOT ALLOW GAPS. Save as `1kmBuffer_20mDepth250mBuffer_Union.shp`.

16) Erase `1kmBuffer_20mDepth250mBuffer_Union.shp` with `DFO_BC_CHS_COASTLIN_AND_BC_BODY.shp` to get `1kmBuffer_20mDepth250mBuffer_UnionErase.shp`

17) Dissolve `1kmBuffer_20mDepth250mBuffer_UnionErase.shp`to get `1kmBuffer_20mDepth250mBuffer_UnionEraseDissolve.shp`

18) Make a copy of `1kmBuffer_20mDepth250mBuffer_UnionEraseDissolve.shp` called `Nearshore_1km20mB.shp`

## Manual Editing to close in inlets

19) Make a copy of `Nearshore_2km_20mB.shp` called `Nearshore_2km_20mB_V2.1.shp`

20) Start edit mode, select all features, and use "edit vertices" tool to select and delete all vertices within the area to be deleted, stopping at the inlet mouth. Save edits.

