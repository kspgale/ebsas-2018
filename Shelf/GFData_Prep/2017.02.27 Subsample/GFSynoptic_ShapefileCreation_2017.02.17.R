#Notes 27 Feb 2017- added total weight per tow for all taxa
#Added shannon layer
#added nSp for presence-absence data (including trace species)
#Update 16 Mar - using summary diversity statistics from previous file. 
#Update 14 July - Create allCPSpecies towXspecies shapefile
#Update 1 Sept - added fix for sandlance name in CP file
#Sept - something about salmon NAs. 
#Sept - add fix for Sandlance species name
# 18 Sept - add species data to Fishing Eventline polylines

#Open files from GF_Synoptic_Processing_2017.02.17

#Make shapefiles from site x species matrixes from gfbio, subsampled so that only one fishing event exists in each grid cell
library(rgdal)
library(maptools)
library(foreign)
library(sp)

#Create total wt per tow spatial layer - for ALL taxa
#Bring in output from GF_Synoptic_Processing
GFSynALLTAXA<-read.csv("E:/Documents/Projects/EBSAs/Analyses/Shelf/GFData_Prep/2017.02.27 Subsample/Outputs/Textfiles/Synoptics//GFSynoptic_Subsampled_TowxWt_AlltaxaInPU_2017.02.28_date.csv")
GFSynALLTAXA2<-GFSynALLTAXA
coordinates(GFSynALLTAXA2)<-~LonMid+LatMid
GFSynALLTAXA2@data$totalWtperHr<-rowSums(GFSynALLTAXA2@data[,c(2:(ncol(GFSynALLTAXA2@data)-2))])
head(GFSynALLTAXA2)[,c(1:5,(ncol(GFSynALLTAXA2)-2):ncol(GFSynALLTAXA2))]
GFSynALLTAXA2<-GFSynALLTAXA2[,(names(GFSynALLTAXA2) %in% c("FISHING_EVENT_ID","totalWtperHr","LatMid", "LonMid"))] #select only these columns
#plot(GFSynALLTAXA2, col=GFSynALLTAXA$totalWtperHr)
# writeSpatialShape(GFSynALLTAXA2, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/GFSynopticSubsample_TotalWtPerHour_AllTaxa.shp")

#Create shapefile of CPs only
#Bring in CP List with aliases (shortened common names)
cp<-read.delim("E:/Documents/Projects/MPAT/3.Conservation Priorities/3.Screen/CPList.txt", sep="\t")
cp$ValidName<-gsub("\\s",".",cp$ValidName)

cp$ValidName[cp$ValidName=="Ammodytes.hexapterus"]<-"Ammodytes.personatus" #Because of an error in the taxonomic table, the GF output file has the wrong name (personatus) for sandlance. Change the CP lookup here so that the shapefile will include it.

GFSynCP<-GFSynALLTAXA[,names(GFSynALLTAXA) %in% c(cp$ValidName, "LonMid","LatMid","Year","Month","ACTIVITY_CODE","FISHING_EVENT_ID")]
GFSynCP$Lat<-GFSynCP$LatMid #duplicate lat and long columns so when we set coordinates() the columns remain in the attribute table
GFSynCP$Lon<-GFSynCP$LonMid
coordinates(GFSynCP)<-~Lon+Lat

#investigate problem with salmon. Even though there are data for pink salmon (for example), the output samefile is all NA
GFSynCP[GFSynCP$Oncorhynchus.gorbuscha>0,] #yes, there are 6 records of pink salmon 
#But for some reason, the following code turns salmon and some others into NAs 

#Create shapefile
proj4string(GFSynCP)<-CRS("+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs") #set coordinate reference system
GFSynCP <- spTransform(GFSynCP, CRS("+proj=aea +lat_1=50 +lat_2=58.5 +lat_0=45 +lon_0=-126 +x_0=1000000 +y_0=0 +datum=NAD83 +units=m +no_defs")) #project
writeOGR(GFSynCP, "E:/Documents/Projects/EBSAs/Analyses/Shelf/GFData_Prep/2017.02.27 Subsample/Outputs/Shapefiles/Synoptics",
         layer=paste("GFSynopticSubsample_CPs"),driver="ESRI Shapefile", overwrite=T )
#rewrite dbf file with the dataframe because (1)writeOGR renames the column names and I hate that, (2) replace the attributes for pink salmon etc with the dataframe version
db<-read.dbf("E:/Documents/Projects/EBSAs/Analyses/Shelf/GFData_Prep/2017.02.27 Subsample/Outputs/Shapefiles/Synoptics/GFSynopticSubsample_CPs.dbf")
db$Oncrhynchs[144] #NULL
db<-GFSynCP
names(db)<-c(names(GFSynCP))
db$Oncorhynchus.gorbuscha[144] #Value!
#Replace full species names with the short names from the CP table
names(db)= cp$Code[match(names(db), cp$ValidName)] #replace the species names
names(db)[c(1,(ncol(db)-4):ncol(db))]<-c("FISHING_EVENT_ID","LatMid","LonMid","Year","Month","ACTIVITY_CODE") #fill in the other names
write.dbf(db,"E:/Documents/Projects/EBSAs/Analyses/Shelf/GFData_Prep/2017.02.27 Subsample/Outputs/Shapefiles/Synoptics/GFSynopticSubsample_CPs.dbf")

#Attach CP data to Lines Shapefile from GFSynoptic_Processing
SynLines<-readOGR("E:/Documents/Projects/EBSAs/Analyses/Shelf/GFData_Prep/2017.02.27 Subsample/LineFiles/Synoptic/SynopticFELines.shp")
names(SynLines)[1]<-"FISHING_EVENT_ID"
head(SynLines@data)
SynopticDb<-read.dbf("E:/Documents/Projects/EBSAs/Analyses/Shelf/GFData_Prep/2017.02.27 Subsample/Outputs/Shapefiles/Synoptics/GFSynopticSubsample_CPs.dbf")
names(SynopticDb)[1]<-"FISHING_EVENT_ID"
SynLines<-merge(SynLines,SynopticDb, by="FISHING_EVENT_ID", all.x=F, all.y=T)
names(SynLines) <- strtrim(names(SynLines),10)
writeOGR(SynLines, dsn="E:/Documents/Projects/EBSAs/Analyses/Shelf/GFData_Prep/2017.02.27 Subsample/LineFiles/Synoptic/Species", layer="SynopticFELinesSp", driver="ESRI Shapefile",overwrite=T)

#OBSOLETE as of 16 mar 2017
# ##Bring in just species-level data
# GFSyn<-read.csv("E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Textfiles/GFSynoptic_Subsampled_TowxWt_SpeciesInPU_2017.02.28.csv")
# dim(GFSyn)
# GFSyn1<-GFSyn #retain original data
#
# #new object for spatial analyses
# GFSyn1$Lat<-GFSyn1$LatMid #add additional columns for lat and long - when you set coordinates, these columns disappear and we want to keep them
# GFSyn1$Lon<-GFSyn1$LonMid
# coordinates(GFSyn1)<-~Lon+Lat
# head(GFSyn1) 
# #Combine the tow numbers with calculated columns for nspecies, shannon metric, etc
# GFSynTaxaOnly<-GFSyn[,c(-1,-(ncol(GFSyn)-1), -ncol(GFSyn))] #remove fishing event column and lat and long
# head(GFSynTaxaOnly)
# #calculate number of specise and shannon index per tow
# NewSummaryWtTow<-data.frame(FISHING_EVENT_ID=GFSyn$FISHING_EVENT_ID,nSp=rowSums(GFSynTaxaOnly>0),shan=diversity(GFSynTaxaOnly, index="shannon")) #note i'm not sure if this is SW or ESW. Add other metrics as wanted
# head(NewSummaryWtTow)
#
# #create richness spatial layer for species that had weights
# Richness<-NewSummaryWtTow[,c(1,2)] #select richnesss column
# GFSynRich<-merge(GFSyn1, Richness, by="FISHING_EVENT_ID") #attach back to spatial data frame
# GFSynRich<-GFSynRich[,(names(GFSynRich) %in% c("FISHING_EVENT_ID","nSp","LatMid", "LonMid"))] #select only these columns
# head(GFSynRich)
# plot(GFSynRich, col=GFSynRich$nSp)
# writeSpatialShape(GFSynRich, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/GFSynopticSubsample_RichnessWEIGHT.shp")
# 
# #create shannon spatial layer for species that had weights
# shannon<-NewSummaryWtTow[,c(1,3)] #select richnesss column
# GFSynShan<-merge(GFSyn1, shannon, by="FISHING_EVENT_ID") #attach back to spatial data frame
# GFSynShan<-GFSynShan[,(names(GFSynShan) %in% c("FISHING_EVENT_ID","shan","LatMid", "LonMid"))] #select only these columns
# head(GFSynShan)
# writeSpatialShape(GFSynShan, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/GFSynopticSubsample_ShannonWEIGHT.shp")

#Create diversity layers from the summarized data, for all taxa that had weights

#for fish
summaryFish<-read.csv("../2017.02.27 Subsample/Outputs/Textfiles/GFSynoptic_Subsampled_diversitySummaryFish_2017.03.16.csv")
head(summaryFish)
coordinates(summaryFish)<-~LonMid+LatMid
fields<-c("nSp","Shan","J_Pielou","ExpShan","E_Heip")

for (i in 1:5){
  subset<-summaryFish[,names(summaryFish) %in% c("FISHING_EVENT_ID",paste(fields[i]),"LatMid", "LonMid")]
  head(subset)
  writeSpatialShape(subset, paste("E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/GFSynopticSubsample_DiversityFISH_WEIGHT_",fields[i],".shp",sep=""))
}

#for Inverts
summaryInverts<-read.csv("../2017.02.27 Subsample/Outputs/Textfiles/GFSynoptic_Subsampled_diversitySummaryInvert_2017.03.16.csv")
head(summaryInverts)
coordinates(summaryInverts)<-~LonMid+LatMid
fields<-c("nSp","Shan","J_Pielou","ExpShan","E_Heip")

for (i in 1:5){
  subset<-summaryInverts[,names(summaryInverts) %in% c("FISHING_EVENT_ID",paste(fields[i]),"LatMid", "LonMid")]
  head(subset)
  writeSpatialShape(subset, paste("E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/GFSynopticSubsample_DiversityINVERT_WEIGHT_",fields[i],".shp",sep=""))
}


#RICHNESS INCLUDING TRACE SPECIES
#Create richness spatial layer for all species (pres-abs)
nSpperTow<-read.csv( "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/TextfilesUPDATED/GFSynoptic_Subsetted_TowxPres_nSpperTow_2017.02.28.csv")
head(nSpperTow)
coordinates(nSpperTow)<-~LonMid+LatMid
writeSpatialShape(nSpperTow, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/GFSynopticSubsample_RichnessPRESENCE-allspp.shp")

#Create richness spatial layer for all FISH species (pres-abs)
nSpperTowFISH<-read.csv( "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/TextfilesUPDATED/GFSynoptic_Subsetted_nSpperTowFish_2017.02.28.csv")
head(nSpperTowFISH)
coordinates(nSpperTowFISH)<-~LonMid+LatMid
writeSpatialShape(nSpperTowFISH, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/GFSynopticSubsample_RichnessPRESENCE-fish.shp")

#Create richness spatial layer for all INVERT species (pres-abs)
nSpperTowINVERT<-read.csv( "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/TextfilesUPDATED/GFSynoptic_Subsetted_nSpperTowInverts_2017.02.28.csv")
head(nSpperTowINVERT)
coordinates(nSpperTowINVERT)<-~LonMid+LatMid
writeSpatialShape(nSpperTowINVERT, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/GFSynopticSubsample_RichnessPRESENCE-invert.shp")

##
NSB<-GFSyn1 #Rename object
##Layers for For individual species
Arrow<-c("FISHING_EVENT_ID","Atheresthes.stomias","LatMid", "LonMid")
NSB_Arrowtooth<-NSB[,(names(NSB) %in% Arrow)]
head(NSB_Arrowtooth@data)
writeSpatialShape(NSB_Arrowtooth, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/GFSynopticSubsample_Arrowtooth.shp")

Bocaccio<-c("FISHING_EVENT_ID","Sebastes.paucispinis","LatMid", "LonMid")
NSB_Bocaccio<-NSB[,(names(NSB) %in% Bocaccio)]
head(NSB_Bocaccio@data)
writeSpatialShape(NSB_Bocaccio, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/GFSynopticSubsample_Bocaccio.shp")

Halibut<-c("FISHING_EVENT_ID","Hippoglossus.stenolepis","LatMid", "LonMid")
NSB_Halibut<-NSB[,(names(NSB) %in% Halibut)]
writeSpatialShape(NSB_Halibut, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/GFSynopticSubsample_Halibut.shp")

Lingcod<-c("FISHING_EVENT_ID","Ophiodon.elongatus","LatMid", "LonMid")
NSB_Lingcod<-NSB[,(names(NSB) %in% Lingcod)]
writeSpatialShape(NSB_Lingcod, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/GFSynopticSubsample_Lingcod.shp")

Pcod<-c("FISHING_EVENT_ID","Gadus.macrocephalus","LatMid", "LonMid")
NSB_Pcod<-NSB[,(names(NSB) %in% Pcod)]
writeSpatialShape(NSB_Pcod, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/GFSynopticSubsample_Pcod.shp")

Rougheye<-c("FISHING_EVENT_ID","Sebastes.aleutianus","LatMid", "LonMid")
NSB_Rougheye<-NSB[,(names(NSB) %in% Rougheye)]
writeSpatialShape(NSB_Rougheye, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/GFSynopticSubsample_Rougheye.shp")

Sablefish<-c("FISHING_EVENT_ID","Anoplopoma.fimbria","LatMid", "LonMid")
NSB_Sablefish<-NSB[,(names(NSB) %in% Sablefish)]
writeSpatialShape(NSB_Sablefish, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/GFSynopticSubsample_Sablefish.shp")

Shortraker<-c("FISHING_EVENT_ID","Sebastes.borealis","LatMid", "LonMid")
NSB_Shortraker<-NSB[,(names(NSB) %in% Shortraker)]
writeSpatialShape(NSB_Shortraker, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/GFSynopticSubsample_Shortraker.shp")

Walleye<-c("FISHING_EVENT_ID","Theragra.chalcogramma","LatMid", "LonMid")
NSB_Walleye<-NSB[,(names(NSB) %in% Walleye)]
writeSpatialShape(NSB_Walleye, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/GFSynopticSubsample_Walleye.shp")

Yelloweye<-c("FISHING_EVENT_ID","Sebastes.ruberrimus","LatMid", "LonMid")
NSB_Yelloweye<-NSB[,(names(NSB) %in% Yelloweye)]
writeSpatialShape(NSB_Yelloweye, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/GFSynopticSubsample_Yelloweye.shp")


