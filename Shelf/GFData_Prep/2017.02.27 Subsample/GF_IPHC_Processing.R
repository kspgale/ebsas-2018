#################################################################
# Formatting of survey data for community analysis
# IPHC Surveys - International Pacific Halibut Commission 
# Katie Gale, 10 April 2017
####

library(data.table)
library(plyr)
library(reshape)
library(vegan)
library(dplyr)
library(lubridate)

#Start with IPHC Longline surveys
#Bring in Data from the GF Bio database
iphc<-data.table(read.csv("E:/Documents/Data files/Data Extractions and Sharing/Groundfish/GFBio_IPHC-Surveys_2017-03-09.txt"))
head(iphc) #19704
iphc$FE_END_DEPLOYMENT_TIME<-as.character(iphc$FE_END_DEPLOYMENT_TIME)
iphc$FE_END_DEPLOYMENT_TIME

date<-as.POSIXct(iphc$FE_END_DEPLOYMENT_TIME, format="%d/%m/%Y %H:%M:%S")
range(date, na.rm=T) #2003.05.27 to 2016.08.13
table(month(date))/197.04

iphc$FE_SP<-paste(iphc$FISHING_EVENT_ID, iphc$SPECIES_CODE,sep="-") #Add ID number to keep track of individual records
iphc[iphc$FE_SP %in% iphc$FE_SP[duplicated(iphc$FE_SP)],][1:20] #Look at duplicate species per fishing event
length(unique(iphc$FE_SP)) #18234 IDs
length(unique(iphc$CATCH_ID)) #19704 - so there are about 1500 records of species weighed twice per tow 

unique(iphc$GEAR_DESC) #all longline

#calculate Coordinates
iphc$LatMid<-((iphc$FE_START_LATTITUDE_DEGREE+(iphc$FE_START_LATTITUDE_MINUTE/60))+(iphc$FE_END_LATTITUDE_DEGREE+(iphc$FE_END_LATTITUDE_MINUTE/60)))/2
iphc$LonMid<-(-1)*((iphc$FE_START_LONGITUDE_DEGREE+(iphc$FE_START_LONGITUDE_MINUTE/60))+(iphc$FE_END_LONGITUDE_DEGREE+(iphc$FE_END_LONGITUDE_MINUTE/60)))/2
plot(iphc$LonMid, iphc$LatMid)

#create list of Fishing events and locations
FE_loc<-iphc[,names(iphc) %in% c("FISHING_EVENT_ID","LatMid","LonMid"), with=FALSE]
FE_loc<-FE_loc[!duplicated(FE_loc),] #2208 fishing events

#change start and end fishing dates into proper format
#To get duration, times must be in "POSIXct" format
iphc$start.fishing<-as.POSIXct(iphc$FE_END_DEPLOYMENT_TIME,format ="%d/%m/%Y %H:%M:%S")
iphc$end.fishing<-as.POSIXct(iphc$FE_BEGIN_RETRIEVAL_TIME,format ="%d/%m/%Y %H:%M:%S")

#Calculate duration of tow in seconds
iphc$fishing.duration.s<-iphc$end.fishing-iphc$start.fishing; units(iphc$fishing.duration.s)<-"secs"
str(iphc$fishing.duration.s) #time in seconds
iphc$fishing.duration.s<-as.numeric(iphc$fishing.duration.s)
summary(iphc$fishing.duration.s)

#add month and year columns
iphc$month<-month(iphc$start.fishing)
iphc$year<-year(iphc$start.fishing)

#Drop those without fishing durations (meaning either start or end fishing times were not available)
iphc2<-iphc[!is.na(iphc$fishing.duration.s),]
nrow(iphc2) #19704 species records 

#start to calculate CPUE - depending on tow and species, some records are count and some are weight
summary(iphc2$CATCH_WEIGHT) #19382 NAs - most of these records are not weighed!
summary(iphc2$CATCH_COUNT) #0 NAs

iphc2$FE_SP<-paste(iphc2$FISHING_EVENT_ID, iphc2$SPECIES_CODE, sep="-") #Create an identifier for each species-tow record
summarybytow<-iphc2[,list(total_wt=sum(CATCH_WEIGHT, na.rm=T), total_count=sum(CATCH_COUNT, na.rm=T)), by=FE_SP] #sum weights and counts for each species-tow (some species were counted more than once per tow)
head(summarybytow) #18234 species records 
length(unique(summarybytow$FE_SP)) #18234 species-tow IDs

#Add the summary to the dataframe
iphc3<-merge(iphc2, summarybytow, by="FE_SP") #19704 
iphc3<-subset(iphc3, !duplicated(FE_SP)) #Remove duplicates (doesn't matter which one) - 18234
head(iphc3)
bothZero<-subset(iphc3, (total_wt==0 & total_count==0)) #check weights and counts that are both zero - 0 records
head(bothZero)

#Calculate CPUE Weight per Hour (kg/hr)
iphc3$WTperHR<-iphc3$total_wt/(iphc3$fishing.duration.s/3600)
iphc3$WTperHR[iphc3$WTperHR==Inf]<-0

#Calculate CPUE Count per Hour (#/hr)
iphc3$CTperHR<-iphc3$total_count/(iphc3$fishing.duration.s/3600)
iphc3$CTperHR[iphc3$CTperHR==Inf]<-0

plot(iphc3$CTperHR)
range(iphc3$fishing.duration.s) #17400-67560 seconds

#Subset columns to be more manageable
iphcDat<-iphc3[,names(iphc3) %in% c("FE_SP","TRIP_ID","ACTIVITY_CODE","TRIP_START_DATE","year", "month","FISHING_EVENT_ID",
                                   "GEAR_DESC","LatMid","LonMid","start.fishing","end.fishing","fishing.duration.s",
                                   "SPECIES_CODE","SPECIES_SCIENCE_NAME","total_wt","total_count","WTperHR","CTperHR"    ), with=F]

#Check records that don't have science names
iphcDat$SPECIES_CODE[iphcDat$SPECIES_SCIENCE_NAME==""] #these M codes are egg cases & can be dropped
iphcDat<-iphcDat[SPECIES_SCIENCE_NAME!="",] #Drop blank names; 18233 records 

##Fix names
#Bring in SpeciesNames from WoRMS 
specNames<-read.delim("E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Inputs/Taxonomy_2017.02.27.txt", sep="\t")
specNames<-specNames[,names(specNames) %in% c("ScientificName","ScientificName_accepted","Phylum","Class","Order")]
names(specNames)[1:2]<-c("OriginalName","ValidName")
specNames<-unique(specNames)

#Make both original and WoRMS lookup names lowercase to match
specNames$OriginalName<-tolower(specNames$OriginalName)
iphcDat$OriginalName<-tolower(iphcDat$SPECIES_SCIENCE_NAME)
unique(iphcDat$OriginalName)

#join corrected names
nrow(iphcDat) #18233
length(unique(iphcDat$FE_SP)) #18233

#change blackspotted to rougheye to make rougheye-blackspotted 
iphcDat$OriginalName[iphcDat$SPECIES_CODE==425]<-"sebastes aleutianus"
iphcDat$OriginalName[iphcDat$SPECIES_CODE==394]

iphcDat2<-merge(as.data.frame(iphcDat), specNames, by="OriginalName", all.x=T, all.y=F)

summary(iphcDat2$ValidName)
#Look at NAs and blanks
unique(iphcDat2$OriginalName[is.na(iphcDat2$ValidName)]) #Acceptable to leave these as NA - remove them
unique(iphcDat2$OriginalName[iphcDat2$ValidName==""])  #These don't have valid names, so remove them. For

#remove NA records
iphcDat3<-data.table(iphcDat2[!is.na(iphcDat2$ValidName),]) #18138
iphcDat3<-data.table(iphcDat3[iphcDat3$ValidName!="",]) #18012

nrow(iphcDat3) #18012
length(unique(iphcDat3$FE_SP)) #18012
iphcDat3[iphcDat3$FE_SP %in% iphcDat3$FE_SP[duplicated(iphcDat3$FE_SP)],]

iphcDat3[iphcDat3$ValidName==""] #check that that none of the names are blank

iphcDat3<-iphcDat3[,!(names(iphcDat3) %in% c("OriginalName","SPECIES_SCIENCE_NAME")),with=FALSE] #Remove these 2 columns

#Bring in file that has taxon level for each name
taxonLevel<-read.csv("E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Inputs/Species_TaxonLevel.csv")
names(taxonLevel)<-c("ValidName","TaxonLevel")
taxonLevel<-unique(taxonLevel)

#Add taxonlevel to the dataframe
iphcDat4<-merge(iphcDat3,taxonLevel, by="ValidName", all.x=T, all.y=F )
nrow(iphcDat4) #18012
iphcDat4$TaxonLevel<-tolower(iphcDat4$TaxonLevel)

#Order columns
setcolorder(iphcDat4, c("FE_SP","TRIP_ID","ACTIVITY_CODE","year", "month","FISHING_EVENT_ID",
                       "GEAR_DESC","LatMid","LonMid","start.fishing","end.fishing","fishing.duration.s",
                       "SPECIES_CODE","ValidName","Phylum","Class","Order","TaxonLevel","total_wt","total_count","WTperHR","CTperHR"))

subset(iphcDat4, is.na(TaxonLevel)) #check for no level - good

length(unique(iphcDat4$ValidName)) #126 taxa

#Create object with only species-level records
iphcDatSpecies<-subset(iphcDat4, TaxonLevel=="species") #16557 records are to species only
length(unique(iphcDat4$ValidName[(iphcDat4$FE_SP %in% iphcDatSpecies$FE_SP)])) #83 species

# 
write.csv(iphcDat4, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Textfiles/GFIPHC_AllTaxa_18012_2017.04.10.csv",row.names=F)
write.csv(iphcDatSpecies, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Textfiles/GFIPHC_Species_16557_2017.04.10.csv",row.names=F)

##########
#Grid data
########
library(maptools)
library(rgdal)
iphcDat4<-read.csv("E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Textfiles/IPHC/GFIPHC_AllTaxa_18012_2017.04.10.csv")

#Start with the dataframe of species records
iphcDat5<-data.table(iphcDat4)
head(iphcDat5)

#Add duplicate columns for Lat and Long - helps later
iphcDat5$LatMid1<-iphcDat5$LatMid
iphcDat5$LonMid1<-iphcDat5$LonMid

#Turn this dataframe into a spatial object
coordinates(iphcDat5)<-~LonMid+LatMid #tell R what the coordates are
proj4string(iphcDat5)<-CRS("+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs") #set coordinate reference system
DatShp <- spTransform(iphcDat5, CRS("+proj=aea +lat_1=50 +lat_2=58.5 +lat_0=45 +lon_0=-126 +x_0=1000000 +y_0=0 +datum=NAD83 +units=m +no_defs")) #project

#import 1 km planning grid and set projection
PUgrid<-readShapeSpatial("E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Inputs/MaPP_PU_Final/MaPP_PU_Final.shp")
proj4string(PUgrid)<-CRS("+proj=aea +lat_1=50 +lat_2=58.5 +lat_0=45 +lon_0=-126 +x_0=1000000 +y_0=0 +datum=NAD83 +units=m +no_defs") #project

####
# spatial join of trawl points with 1km grid - overlay could take a long time
#####
overlay<-over(DatShp,PUgrid) #Spatial join our dataset with the grids
DatwGrid<-cbind(DatShp@data,overlay ) #Bind the species data with the overlay info 

DatwGrid<-DatwGrid[,!(names(DatwGrid) %in% c("OBJECTID_1","Shape_Le_1","Shape_Area")) ] #drop unwanted columns from grid shapefile
head(DatwGrid)

#Create a shapefile that has just the PU grids that contain data
#if gridID=NA, then that point was outside of our study area
populated<-(unique(DatwGrid$PU_ID)) #get list of populated grid cells
length(unique(PUgrid$PU_ID)) #113839 grid cells initiallly 
gridSubAllTaxa<-subset(PUgrid,PUgrid$PU_ID %in% populated) #select only those grid cells that our data populates
length(unique(gridSubAllTaxa$PU_ID)) #   356 grid cells are populated by all taxa 
head(gridSubAllTaxa@data)

#Write this shapefile for later use
# writeSpatialShape(gridSubAllTaxa,"E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/PU_with_Data_IPHC_2017.04.10.shp") 


DatwGrid2<-DatwGrid[!is.na(DatwGrid$PU_ID),] #Drop records that are outside the planning region (14943 records remain)
head(DatwGrid2)

#Look at planning units that have multiple surveys in them
#Select planning units that have more than 1 fishing event

PU_FE<-cbind.data.frame(DatwGrid2$FISHING_EVENT_ID,DatwGrid2$PU_ID)
PU_FE<-unique(PU_FE)
names(PU_FE)<-c("FISHING_EVENT_ID","PU_ID")
head(PU_FE) #all fishing events with associated planning unit

PUCount<-ddply(PU_FE,  c("PU_ID"), summarize,nFEinPU=length(unique(FISHING_EVENT_ID)))
head(PUCount)

#Output - number of fishing evnets per planning unit
PUGridCount<-merge(PUgrid,PUCount,  by="PU_ID")
head(PUGridCount@data)
PUGridCount@data$nFEinPU[is.na(PUGridCount@data$nFEinPU)]<-0
# writeSpatialShape(PUGridCount, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/IPHC_NFEperPU.shp")

dupPU<-subset(PUCount, nFEinPU>1) #select planning units that have more than one fishing event in them
dupPU  #231 planning units have more than one fishing event (of 356)

#Instead of subsampling the Planning units, as we did for the synoptics, summarize the catch within each PU (average, SD, and CV)
head(DatwGrid2)
co.var<-function(x) 100*(sd(x,na.rm=TRUE)/mean(x,na.rm=TRUE))
PUSummary<-ddply(DatwGrid2, c("PU_ID","ValidName"), summarize, CTperHR_avg=mean(CTperHR, na.rm=T), CTperHR_SD=sd(CTperHR, na.rm=T), CTperHR_CV=co.var(CTperHR)  )
head(PUSummary)

PUSummary[PUSummary$PU_ID==106831,]
PUSummary[PUSummary$PU_ID==3012,] #NA SD and count because only one FE in PU
PUGridCount@data[PUGridCount@data$PU_ID==3012,]

#################################
#Get PU x Species Matrices
################################

DatwGridCTforMat<-PUSummary[,c("PU_ID","ValidName","CTperHR_avg")]

#SITE (planning unit) x SPECIES MATRICES
#Cast data into site x species
gridCastCt1<-cast(DatwGridCTforMat,PU_ID~ValidName,value="CTperHR_avg", fun=mean, na.rm=T) #average relative abundances per grid cell (should be the value in the PUSummary table since there should now be only one record per species per PU)
head(gridCastCt1)
gridCastCt1[is.na(gridCastCt1)]<-0 #Replace NaNs with zeros
dim(gridCastCt1) #356sites x 125 sp (first column is PU_ID)
write.csv(gridCastCt1, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Textfiles/IPHC/GFIPHC_PUxCt_AllTaxa_2017.04.10.csv",row.names=F)

DatwGridCTforMatSD<-PUSummary[,c("PU_ID","ValidName","CTperHR_SD")]
#SITE (planning unit) x SPECIES MATRICES
#Cast data into site x species
gridCastCt1SD<-cast(DatwGridCTforMatSD,PU_ID~ValidName,value="CTperHR_SD", fun=mean, na.rm=T) #average relative abundances per grid cell (should be the value in the PUSummary table since there should now be only one record per species per PU)
head(gridCastCt1SD)
gridCastCt1SD[is.na(gridCastCt1SD)]<-0 #Replace NaNs with zeros
dim(gridCastCt1SD) #356sites x 125 sp (first column is PU_ID)
write.csv(gridCastCt1SD, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Textfiles/IPHC/GFIPHC_PUxCtSD_AllTaxa_2017.04.10.csv",row.names=F)


################################
#TOW X SPECIES Matrices
################################
FELoc<-unique(cbind.data.frame(DatwGrid2$FISHING_EVENT_ID, DatwGrid2$LatMid1, DatwGrid2$LonMid1) ) #fishing event locations
names(FELoc)<-c("FISHING_EVENT_ID","LatMid","LonMid")
head(FELoc)

# #Tow by species count
perSetCt<-DatwGrid2[,c("FISHING_EVENT_ID","ValidName","CTperHR")]
head(perSetCt)
perSetCtMelt<-melt(perSetCt, id.vars=c("FISHING_EVENT_ID","ValidName"))
perSetCtCast<-cast(perSetCtMelt, FISHING_EVENT_ID~ValidName, fun=sum) #site x species
dim(perSetCtCast) #1779 x 125 species + 1 col
perSetCtCast2<-join(perSetCtCast, FELoc, by="FISHING_EVENT_ID") #attach locations
dim(perSetCtCast2) #1779 x 125 species + 3 col
# write.csv(perSetCtCast2, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Textfiles/GFIPHC_TowxCt_TaxaInPU_2017.04.10.csv",row.names=F)

#######
#Shapefile Creation
#########
library(rgdal)
library(maptools)
# perSetCtCast2<-read.csv("E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Textfiles/IPHC/GFIPHC_TowxCt_SpeciesInPU_2017.04.10.csv")
# gridCastCt1SD<-read.csv("E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Textfiles/IPHC/GFIPHC_PUxCtSD_AllTaxa_2017.04.10.csv")
# gridCastCt1<-read.csv("E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Textfiles/IPHC/GFIPHC_PUxCt_AllTaxa_2017.04.10.csv")
# gridSubAllTaxa<-readShapeSpatial("E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/IPHC/PU_with_Data_IPHC_2017.04.10.shp") 

GFIPHCshp<-perSetCtCast2
coordinates(GFIPHCshp)<-~LonMid+LatMid

#Do for Tows and for PUs
#Layers for For individual species
Sable<-c("FISHING_EVENT_ID","PU_ID","Anoplopoma fimbria","LatMid", "LonMid")
Sable_PU_SD<-gridCastCt1SD[,(names(gridCastCt1SD) %in% Sable)]
head(Sable_PU_SD)
Sable_PU_SD_shp<-merge(gridSubAllTaxa,Sable_PU_SD, by="PU_ID")
writeSpatialShape(Sable_PU_SD_shp, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/IPHC/GFIPHC_CtSD_PU_Sablefish.shp")
Sable_PU<-gridCastCt1[,(names(gridCastCt1) %in% Sable)]
head(Sable_PU)
Sable_PU_shp<-merge(gridSubAllTaxa,Sable_PU, by="PU_ID")
writeSpatialShape(Sable_PU_shp, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/IPHC/GFIPHC_CtAvg_PU_Sablefish.shp")
Sable_tow<-GFIPHCshp[,(names(GFIPHCshp) %in% Sable)]
head(Sable_tow)
writeSpatialShape(Sable_tow, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/IPHC/GFIPHC_CtTow_Sablefish.shp")

Halibut<-c("ING_EVENT_ID","PU_ID","Hippoglossus stenolepis","LatMid", "LonMid")
Halibut_PU_SD<-gridCastCt1SD[,(names(gridCastCt1SD) %in% Halibut)]
Halibut_PU_SD_shp<-merge(gridSubAllTaxa,Halibut_PU_SD, by="PU_ID")
writeSpatialShape(Halibut_PU_SD_shp, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/IPHC/GFIPHC_CtSD_PU_Halibut.shp")
Halibut_PU<-gridCastCt1[,(names(gridCastCt1) %in% Halibut)]
Halibut_PU_shp<-merge(gridSubAllTaxa,Halibut_PU, by="PU_ID")
writeSpatialShape(Halibut_PU_shp, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/IPHC/GFIPHC_CtAvg_PU_Halibut.shp")
Halibut_tow<-GFIPHCshp[,(names(GFIPHCshp) %in% Halibut)]
writeSpatialShape(Halibut_tow, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/IPHC/GFIPHC_CtTow_Halibut.shp")

Yelloweye<-c("ING_EVENT_ID","PU_ID","Sebastes ruberrimus","LatMid", "LonMid")
Yelloweye_PU_SD<-gridCastCt1SD[,(names(gridCastCt1SD) %in% Yelloweye)]
Yelloweye_PU_SD_shp<-merge(gridSubAllTaxa,Yelloweye_PU_SD, by="PU_ID")
writeSpatialShape(Yelloweye_PU_SD_shp, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/IPHC/GFIPHC_CtSD_PU_Yelloweye.shp")
Yelloweye_PU<-gridCastCt1[,(names(gridCastCt1) %in% Yelloweye)]
Yelloweye_PU_shp<-merge(gridSubAllTaxa,Yelloweye_PU, by="PU_ID")
writeSpatialShape(Yelloweye_PU_shp, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/IPHC/GFIPHC_CtAvg_PU_Yelloweye.shp")
Yelloweye_tow<-GFIPHCshp[,(names(GFIPHCshp) %in% Yelloweye)]
writeSpatialShape(Yelloweye_tow, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/IPHC/GFIPHC_CtTow_Yelloweye.shp")

POP<-c("ING_EVENT_ID","PU_ID","Sebastes alutus","LatMid", "LonMid")
POP_PU_SD<-gridCastCt1SD[,(names(gridCastCt1SD) %in% POP)]
POP_PU_SD_shp<-merge(gridSubAllTaxa,POP_PU_SD, by="PU_ID")
writeSpatialShape(POP_PU_SD_shp, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/IPHC/GFIPHC_CtSD_PU_POP.shp")
POP_PU<-gridCastCt1[,(names(gridCastCt1) %in% POP)]
POP_PU_shp<-merge(gridSubAllTaxa,POP_PU, by="PU_ID")
writeSpatialShape(POP_PU_shp, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/IPHC/GFIPHC_CtAvg_PU_POP.shp")
POP_tow<-GFIPHCshp[,(names(GFIPHCshp) %in% POP)]
writeSpatialShape(POP_tow, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/IPHC/GFIPHC_CtTow_POP.shp")

Redstripe<-c("ING_EVENT_ID","PU_ID","Sebastes proriger","LatMid", "LonMid")
Redstripe_PU_SD<-gridCastCt1SD[,(names(gridCastCt1SD) %in% Redstripe)]
Redstripe_PU_SD_shp<-merge(gridSubAllTaxa,Redstripe_PU_SD, by="PU_ID")
writeSpatialShape(Redstripe_PU_SD_shp, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/IPHC/GFIPHC_CtSD_PU_Redstripe.shp")
Redstripe_PU<-gridCastCt1[,(names(gridCastCt1) %in% Redstripe)]
Redstripe_PU_shp<-merge(gridSubAllTaxa,Redstripe_PU, by="PU_ID")
writeSpatialShape(Redstripe_PU_shp, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/IPHC/GFIPHC_CtAvg_PU_Redstripe.shp")
Redstripe_tow<-GFIPHCshp[,(names(GFIPHCshp) %in% Redstripe)]
writeSpatialShape(Redstripe_tow, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/IPHC/GFIPHC_CtTow_Redstripe.shp")

Yellowmouth<-c("ING_EVENT_ID","PU_ID","Sebastes reedi","LatMid", "LonMid")
Yellowmouth_PU_SD<-gridCastCt1SD[,(names(gridCastCt1SD) %in% Yellowmouth)]
Yellowmouth_PU_SD_shp<-merge(gridSubAllTaxa,Yellowmouth_PU_SD, by="PU_ID")
writeSpatialShape(Yellowmouth_PU_SD_shp, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/IPHC/GFIPHC_CtSD_PU_Yellowmouth.shp")
Yellowmouth_PU<-gridCastCt1[,(names(gridCastCt1) %in% Yellowmouth)]
Yellowmouth_PU_shp<-merge(gridSubAllTaxa,Yellowmouth_PU, by="PU_ID")
writeSpatialShape(Yellowmouth_PU_shp, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/IPHC/GFIPHC_CtAvg_PU_Yellowmouth.shp")
Yellowmouth_tow<-GFIPHCshp[,(names(GFIPHCshp) %in% Yellowmouth)]
writeSpatialShape(Yellowmouth_tow, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/IPHC/GFIPHC_CtTow_Yellowmouth.shp")

Shortspine<-c("ING_EVENT_ID","PU_ID","Sebastolobus alascanus","LatMid", "LonMid")
Shortspine_PU_SD<-gridCastCt1SD[,(names(gridCastCt1SD) %in% Shortspine)]
Shortspine_PU_SD_shp<-merge(gridSubAllTaxa,Shortspine_PU_SD, by="PU_ID")
writeSpatialShape(Shortspine_PU_SD_shp, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/IPHC/GFIPHC_CtSD_PU_Shortspine.shp")
Shortspine_PU<-gridCastCt1[,(names(gridCastCt1) %in% Shortspine)]
Shortspine_PU_shp<-merge(gridSubAllTaxa,Shortspine_PU, by="PU_ID")
writeSpatialShape(Shortspine_PU_shp, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/IPHC/GFIPHC_CtAvg_PU_Shortspine.shp")
Shortspine_tow<-GFIPHCshp[,(names(GFIPHCshp) %in% Shortspine)]
writeSpatialShape(Shortspine_tow, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/IPHC/GFIPHC_CtTow_Shortspine.shp")


