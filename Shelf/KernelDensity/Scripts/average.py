# Name: reclassSyn.py
# Description: Take the SUM and MEAN a stack of rasters.
# Author: Katie Gale
# Date: 2 August 2017


# Import system modules
import numpy
import arcpy
from arcpy import env
from arcpy.sa import *

# Set environment settings
env.workspace = "E:/Documents/Projects/EBSAs/Analyses/Shelf/KernelDensity/20kmNeighbour_pooled/2.ReclassifyPercentile"
inwk="E:/Documents/Projects/EBSAs/Analyses/Shelf/KernelDensity/20kmNeighbour_pooled/2.ReclassifyPercentile/"
outwk="E:/Documents/Projects/EBSAs/Analyses/Shelf/KernelDensity/20kmNeighbour_pooled/3.Stack/"

#Set parameters
#survNo='27'
survNo=''
#surv = [fc for fc in arcpy.ListRasters() if fc.startswith('kernel_Syn_Sub_TowxWt_'+survNo)]
surv = [fc for fc in arcpy.ListRasters() if fc.startswith('kernel_')]

#set -128 (null) to NODATA and stack the rasters
rasterlist = []
for i in xrange(0,len(surv)):
    raster_dataset = arcpy.Raster(inwk+surv[i])
    raster_dataset2=Con(raster_dataset,raster_dataset,"","VALUE>-128")
    rasterlist.append(raster_dataset2)

outCellMean = CellStatistics(rasterlist, "MEAN", "DATA")
outCellSum = CellStatistics(rasterlist, "SUM", "DATA")

outfileMean=outwk+survNo+"_mean"+".tif"
outfileSum=outwk+survNo+"_sum"+".tif"
outCellMean.save(outfileMean)
outCellSum.save(outfileSum)

#new=Con(outCellMean,outCellMean, "", outCellSum == outCellMean)
