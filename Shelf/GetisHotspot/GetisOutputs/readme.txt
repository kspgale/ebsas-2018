Differences in shapefiles and processing steps:

1. SURVEY_Hotspot_SPECIES_Richness.shp and _Shannon.shp: output of Getis-Ord analysis. 
	Points with values of richness or diversity from each survey were analyzed. 
	Outputs are hotspot values (field Gi_Bin) -3 to +3.
2. _123.shp: Extracted positive Gi_Bin values only (1, 2, 3).
3. _123_MBG.shp: minimum bounding geometry around 123.shp.
4. _123_MBG_1kmB.shp: minumum bounding geometry around 123.shp with 1 km buffer. This is the final version used in EBSA paper.